import React from 'react';
import {StyleSheet, Text, View, Platform, StatusBar} from 'react-native';
import {TabNavigator, StackNavigator} from 'react-navigation'
import {Constants} from 'expo'
import {purple, white} from './utils/colors'
import { setLocalNotification } from './utils/helpers'
import {FontAwesome, Ionicons, MaterialCommunityIcons, Foundation} from '@expo/vector-icons';
import {Provider} from 'react-redux';
import store from './store';
import MainNavigator from './navigation/MainNavigator';


function MainStatusBar({backgroundColor, ...props}) {
    return (
        <View style={{backgroundColor, height: Constants.statusBarHeight}}>
            <StatusBar translucent backgroundColor={backgroundColor} {...props} />
        </View>
    )
}



export default class App extends React.Component {

    componentDidMount() {
        setLocalNotification();
    }


    render() {
        return (
            <Provider store={store}>
                <View style={{flex: 1}}>
                    <MainStatusBar backgroundColor={purple} barStyle="light-content"/>
                    <MainNavigator/>
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
