import React from 'react';
import {AsyncStorage} from 'react-native'
import {Notifications, Permissions} from 'expo';
import * as constant from './constans';




const createNotification = (title, body)=> {
    return {
        title: title,
        body: body,
        ios: {
            sound: true,
        },
        android: {
            sound: true,
            priority: 'high',
            sticky: false,
            vibrate: true,
        }
    }

}

export function clearLocalNotification() {
    return AsyncStorage.removeItem(constant.STUDY_REMINDER_KEY)
        .then(Notifications.cancelAllScheduledNotificationsAsync)
}

export function setLocalNotification() {
    AsyncStorage.getItem(constant.STUDY_REMINDER_KEY)
        .then(JSON.parse)
        .then((data) => {
            if (data === null) {
                Permissions.askAsync(Permissions.NOTIFICATIONS)
                    .then(({status}) => {
                        if (status === 'granted') {
                            Notifications.cancelAllScheduledNotificationsAsync();
                            let tomorrow = new Date();
                            tomorrow.setDate(tomorrow.getDate() + 1);
                            tomorrow.setHours(13);
                            tomorrow.setMinutes(0);
                            Notifications.scheduleLocalNotificationAsync(
                                createNotification('Study Reminder!', 'Don\'t forget to study today!'),
                                {
                                    time: tomorrow,
                                    repeat: 'day',
                                }
                            )
                            AsyncStorage.setItem(constant.STUDY_REMINDER_KEY, JSON.stringify(true))
                        }
                    })
            }
        })
}
