import {AsyncStorage} from 'react-native';
import {DECK_STORAGE_KEY} from './constans';


export function getDecks() {
    return AsyncStorage.getItem(DECK_STORAGE_KEY)
        .then(formatResult)
}

export function getDeck(id) {
    AsyncStorage.getItem(DECK_STORAGE_KEY)
        .then(formatResult)
        .then((results) => results[id])
}

export function saveDeckTitle(title) {
    return AsyncStorage.mergeItem(DECK_STORAGE_KEY, JSON.stringify({
        [title]: {
            title: title,
            questions: []
        }
    }))

}

export function addCardToDeck(title, card) {
    return AsyncStorage.getItem(DECK_STORAGE_KEY)
        .then(formatResult)
        .then((result) => {
            result[title].questions.push(card);
            console.log(result[title]);
             AsyncStorage.setItem(DECK_STORAGE_KEY, JSON.stringify(result))
        });


}


function formatResult(results) {
    return results === null
        ? setDummyData()
        : JSON.parse(results)
}

function setDummyData() {
    let dummyData = {
        React: {
            title: 'React',
            questions: [
                {
                    question: 'What is React?',
                    answer: 'A library for managing user interfaces'
                },
                {
                    question: 'Where do you make Ajax requests in React?',
                    answer: 'The componentDidMount lifecycle event'
                }
            ]
        },
        JavaScript: {
            title: 'JavaScript',
            questions: [
                {
                    question: 'What is a closure?',
                    answer: 'The combination of a function and the lexical environment within which that function was declared.'
                }
            ]
        }
    };

    AsyncStorage.setItem(DECK_STORAGE_KEY, JSON.stringify(dummyData))

    return dummyData;
}