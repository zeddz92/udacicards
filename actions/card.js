import * as actionTypes from './types';


export const saveCard = (title, card) => (dispatch, getState, api) => (
    api
        .addCardToDeck(title, card)
        .then(decks => dispatch(addCard({title, card})))
);


export const addCard = deck => ({
    type: actionTypes.ADD_CARD,
    payload: deck,
    error: false
});
