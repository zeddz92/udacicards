import * as actionTypes from './types';

export const fetchDecks = () => (dispatch, getState, api) => (
    api
        .getDecks()
        .then(decks => dispatch(receiveDecks({decks})))
);

export const receiveDecks = decks => ({
    type: actionTypes.RECEIVE_DECKS,
    payload: decks,
    error: false

});

export const saveDeck = (title) => (dispatch, getState, api) => (
    api
        .saveDeckTitle(title)
        .then(() => dispatch(addDeck({title})))
);

export const addDeck = deck => ({
    type: actionTypes.ADD_DECK,
    payload: deck,
    error: false
});




