import React from 'react';
import DeckView from '../components/DeckView';
import Quiz from '../components/Quiz';
import AddCard from '../components/AddCard';
import {StackNavigator} from 'react-navigation';
import {purple, white} from '../utils/colors';
import HomeTabs from './HomeTabs';


const MainNavigator = StackNavigator({
    Home: {
        screen: HomeTabs,
    },
    DeckView: {
        screen: DeckView,
        navigationOptions: {
            headerTintColor: white,
            headerStyle: {
                backgroundColor: purple,
            }
        }
    },
    Quiz: {
        screen: Quiz,
        navigationOptions: {
            title: 'Quiz',
            headerTintColor: white,
            headerStyle: {
                backgroundColor: purple,
            }
        }
    },
    AddCard: {
        screen: AddCard,
        navigationOptions: {
            title: 'Add Card',
            headerTintColor: white,
            headerStyle: {
                backgroundColor: purple,
            }
        },
    },
});

export default MainNavigator;