import NewDeck from '../components/NewDeck';
import React from 'react';
import DeckList from '../components/DeckList';
import {MaterialCommunityIcons, Foundation} from '@expo/vector-icons';
import {TabNavigator} from 'react-navigation';
import {Platform} from 'react-native';
import {purple, white} from '../utils/colors'


const HomeTabs = TabNavigator({
    DeckList: {
        screen: DeckList,
        navigationOptions: {
            tabBarLabel: 'Decks',
            tabBarIcon: ({tintColor}) => <MaterialCommunityIcons name='cards-playing-outline' size={30}
                                                                 color={tintColor}/>
        },
    },
    NewDeck: {
        screen: NewDeck,
        navigationOptions: {
            tabBarLabel: 'New Deck',
            tabBarIcon: ({tintColor}) => <Foundation name='page-add' size={30} color={tintColor}/>

        },
    },
}, {
    navigationOptions: {
        header: null
    },
    tabBarOptions: {
        activeTintColor: Platform.OS === 'ios' ? purple : white,
        style: {
            height: 56,
            backgroundColor: Platform.OS === 'ios' ? white : purple,
            shadowColor: 'rgba(0, 0, 0, 0.24)',
            shadowOffset: {
                width: 0,
                height: 3
            },
            shadowRadius: 6,
            shadowOpacity: 1
        }
    }
});

export default HomeTabs;