import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import * as API from './utils/api';
import {persistStore, persistCombineReducers} from 'redux-persist'
import storage from 'redux-persist/es/storage' // default: localStorage if web, AsyncStorage if react-native

const config = {
    key: 'root',
    storage,
}

const logger = store => next => action => {
    // console.group(action.type);
    // console.info('dispatching', action);
    let result = next(action);
    // console.log('next state', store.getState());
    // console.groupEnd(action.type);
    return result
};

// const reducer = persistCombineReducers(config, reducers);

const reducer = persistCombineReducers(config, reducers);



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk.withExtraArgument(API)),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

persistStore(store);


export default store;
