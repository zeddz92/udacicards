import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from  'react-native';
import * as color from '../utils/colors';


function SubmitBtn(props) {
    const {onPress} = props;
    return (
        <TouchableOpacity
            style={styles.submitBtn}
            onPress={onPress}>
            <Text style={styles.submitBtnText}>SUBMIT</Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    submitBtn: {
        backgroundColor: color.black,
        padding: 10,
        width: 200,
        borderRadius: 7,
        height: 45,
        marginLeft: 40,
        marginRight: 40,
    },
    submitBtnText: {
        color: color.white,
        fontSize: 22,
        textAlign: 'center',
    },
});

export default SubmitBtn;