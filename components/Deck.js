import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export default class Deck extends Component {
    render() {
        const {deck} = this.props;
        return (
            <View style={{alignItems:'center', marginBottom: 15}}>
                <Text style={styles.title} >{deck.title}</Text>
                <Text style={styles.cardNumber} >{deck.questions.length} cards</Text>
            </View>

        );
    }
}


const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 35,
        marginBottom:5,
    },
    cardNumber: {
        fontSize: 15,
        color: '#747574'
    },
});
