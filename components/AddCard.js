import React, {Component} from 'react';
import SubmitBtn from './SubmitBtn';
import {View, Text, StyleSheet, KeyboardAvoidingView, TextInput} from 'react-native';
import {connect} from 'react-redux';
import * as cardActions from '../actions/card';
import {Form, TextValidator} from 'react-native-validator-form';
import * as color from '../utils/colors';



class AddCard extends Component {
    state = {
        question: "",
        answer: "",
        questionError: false,
        answerError: false
    };

    handleSubmit = () => {
        let form = this.refs.form.submit();
    };


    handleTextChange = (key, text) => {
        this.setState({
            [key]: text,
            [`${key}Error`]: false

        });
    };

    handleErrors = (errors) => {
        errors.map((error) => {
            this.setState({[error.props.errorLabel]: true});
        })

    }


    submit = () => {
        const {title} = this.props.navigation.state.params;
        const card = this.state;
        this.props.saveCard(title, card).then(() => {
            this.props.navigation.goBack();
        })
    };


    render() {
        const {question, answer, questionError, answerError} = this.state;
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <Form
                    onError={(errors) => this.handleErrors(errors)}
                    ref="form"
                    onSubmit={this.submit}>
                    <TextValidator name="question"
                                   errorLabel="questionError"
                                   validators={['required']}
                                   errorMessages={['This field is required']}
                                   placeholder="Card Question"
                                   value={question}
                                   style={[styles.input, {borderColor: questionError ? color.red : color.black}]}
                                   onChangeText={(text) => this.handleTextChange("question", text)}
                                   type="text"/>

                    <TextValidator name="answer"
                                   errorLabel="answerError"
                                   validators={['required']}
                                   errorMessages={['This field is required']}
                                   placeholder="Card Answer"
                                   value={answer}
                                   style={[styles.input, {borderColor: answerError ? color.red : color.black}]}
                                   onChangeText={(text) => this.handleTextChange("answer", text)}
                                   type="text"/>
                </Form>


              {/*  <TextInput onChangeText={(text) => this.handleTextChange("question", text)} value={question}
                           placeholder="Card Question"
                           style={styles.input}/>

                <TextInput onChangeText={(text) => this.handleTextChange("answer", text)} value={answer}
                           placeholder="Card Answer"
                           style={styles.input}/>*/}

                <SubmitBtn onPress={this.handleSubmit}/>
            </KeyboardAvoidingView>


        );
    }
}


const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        flex: 1,
        alignItems: 'center',

    },
    input: {
        width: 300,
        height: 44,
        padding: 8,
        borderWidth: 1,
        borderColor: '#757575',
        marginTop: 20,
        marginBottom: 50,

    },
});


function mapDispatchToProps(dispatch) {
    return {
        saveCard: (title, card) => dispatch(cardActions.saveCard(title, card))
    }
}


export default connect(
    null,
    mapDispatchToProps
)(AddCard)
