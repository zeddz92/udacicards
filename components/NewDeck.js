import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Dimensions, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import * as color from '../utils/colors';
import * as deckActions from '../actions/deck';
import {connect} from 'react-redux';
import {Form, TextValidator} from 'react-native-validator-form';
import {Constants} from 'expo';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function SubmitBtn({onPress}) {

    return (
        <TouchableOpacity
            style={styles.submitBtn}
            onPress={onPress}>
            <Text style={styles.submitBtnText}>SUBMIT</Text>
        </TouchableOpacity>
    )
}

class NewDeck extends Component {
    state = {
        title: "",
        titleError: false,
    };

    handleTextChange = (title) => {
        this.setState({
            title,
            titleError: false
        });
    };

    handleSubmit = () => {
        this.refs.form.submit();
    };

    submit = () => {
        const {title} = this.state;
        this.setState({
            title: "",
            titleError: false
        });

        this.props.saveDeck(title).then(() => {
            this.props.navigation.navigate('DeckList');
        });
    };

    handleErrors = (errors) => {
        errors.map((error) => {
            this.setState({[error.props.errorLabel]: true});
        })
    };


    render() {
        const {title, titleError} = this.state;
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <Form
                    onError={this.handleErrors}
                    ref="form"
                    onSubmit={this.submit}>
                    <Text style={styles.titleLabel}>What is the title of your new deck</Text>
                    <View style={{flexDirection: 'row', justifyContent:'center'}}>
                        <TextValidator name="title"
                                       errorLabel="titleError"
                                       validators={['required']}
                                       errorMessages={['This field is required']}
                                       placeholder="Deck Title"
                                       value={title}
                                       style={[styles.input, {borderColor: titleError ? color.red : color.black}]}
                                       onChangeText={(text) => this.handleTextChange(text)}
                                       type="text"/>
                    </View>

                </Form>

                <SubmitBtn onPress={this.handleSubmit}/>
            </KeyboardAvoidingView>
        );
    }
}
function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        margin: 30,
        justifyContent: 'center'
    },

    titleLabel: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 50,
    },
    submit: {
        backgroundColor: color.black,
        width: 100
    },
    input: {
        width: wp(75),
        height: 44,
        padding: 8,
        borderWidth: 1,
        borderColor: '#757575',
        marginTop: 20,
        marginBottom: 50,

    },
    divider: {
        width: 1000,
        marginBottom: 50,
        marginTop: 50,
        borderColor: '#EF6B00',
        height: 1,
        backgroundColor: '#EF6B00'
    },
    submitBtn: {
        backgroundColor: color.black,
        padding: 10,
        width: 200,
        borderRadius: 7,
        height: 45,
        marginLeft: 40,
        marginRight: 40,
    },
    submitBtnText: {
        color: color.white,
        fontSize: 22,
        textAlign: 'center',
    },
});


function mapDispatchToProps(dispatch) {
    return {
        saveDeck: (title) => dispatch(deckActions.saveDeck(title))
    }
}

export default connect(
    null,
    mapDispatchToProps
)(NewDeck)
