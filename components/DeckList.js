import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, FlatList, Platform} from 'react-native';
import {connect} from 'react-redux';
import * as deckActions from '../actions/deck';
import * as color from '../utils/colors';



class DeckList extends Component {

    componentDidMount() {
        this.props.fetchDecks();
    }

    toDetail(deck) {
        this.props.navigation.navigate(
            'DeckView',
            {title: deck.title}
        )
    }

    renderItem = ({item}) => {
        return (
            <View style={styles.item} >
                <TouchableOpacity onPress={() => this.toDetail(item)}>
                    <View style={{alignItems:'center'}}>
                        <Text style={styles.title} >{item.title}</Text>
                        <Text style={styles.cardNumber} >{item.questions.length} cards</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };


    render() {


        const {decks} = this.props;
        return (
            <View>
                <FlatList
                    data={decks}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index}
                />


            </View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: color.white,
        borderRadius: Platform.OS === 'ios' ? 16 : 2,
        padding: 20,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 17,
        justifyContent: 'center',
        shadowRadius: 3,
        shadowOpacity: 0.8,
        shadowColor: 'rgba(0, 0, 0, 0.24)',
        shadowOffset: {
            width: 0,
            height: 3
        },
    },

    card: {
        shadowOffset:{  width: 10,  height: 10,  },
        shadowColor: 'black',
        shadowOpacity: 1.0,
    },
    divider: {
        marginTop: 50,
        marginBottom: 50,
        borderColor: '#EF6B00',
        height: 1,
        backgroundColor: '#EF6B00'
    },
    title: {
        fontSize: 25
    },
    cardNumber: {
        color: '#747574'
    },
})


function mapStateToProps({deck}) {
    return {
        decks: Object.keys(deck).map((key) => (
            deck[key]
        ))
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchDecks: () => dispatch(deckActions.fetchDecks())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DeckList)
