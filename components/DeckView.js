import React, {PureComponent} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Deck from './Deck';
import * as color from '../utils/colors';
import {connect} from 'react-redux';


class DeckView extends PureComponent {
    static navigationOptions = ({navigation}) => {
        const {title} = navigation.state.params;
        return {
            title: title
        }
    };

    toAddCard = (deck) => {
        this.props.navigation.navigate(
            'AddCard',
            {title: deck.title}
        )
    };

    toQuiz = (deck) => {
        this.props.navigation.navigate(
            'Quiz',
            {deck: deck}
        )
    };

    render() {
        const {deck} = this.props;
        return (
            <View style={styles.container}>
                <Deck deck={deck}>DeckView</Deck>

                <View style={styles.center}>

                    <TouchableOpacity onPress={() => this.toAddCard(deck)} style={styles.addCardBtn}>
                        <Text style={styles.buttonText}>Add card</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.toQuiz(deck)} style={styles.submitBtn}>
                        <Text style={[styles.buttonText, {color: color.white}]}>Start Quiz</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        margin: 30,
        justifyContent: 'center'
    },

    center: {
        marginTop: 180,
        alignItems: 'center',
        justifyContent: 'center'
    },

    submit: {
        backgroundColor: color.black,
        width: 100
    },
    input: {
        width: 300,
        height: 44,
        padding: 8,
        borderWidth: 1,
        borderColor: '#757575',
        margin: 50

    },
    buttonText: {
        textAlign: 'center',
    },
    submitBtn: {
        backgroundColor: color.black,
        padding: 10,
        width: 200,
        borderRadius: 7,
        height: 45,
        marginLeft: 40,
        marginRight: 40,
    },
    addCardBtn: {
        backgroundColor: color.white,
        borderColor: color.black,
        borderWidth: 1,
        padding: 10,
        width: 200,
        borderRadius: 7,
        height: 45,
        marginLeft: 40,
        marginRight: 40,
        marginBottom: 20
    },
    submitBtnText: {
        color: color.white,
        fontSize: 22,
        textAlign: 'center',
    },
});

function mapStateToProps({deck}, ownProps) {
    const {title} = ownProps.navigation.state.params;
    return {
        deck: deck[title]
    }
}

export default connect(
    mapStateToProps
)(DeckView)

