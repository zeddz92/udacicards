import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Animated, Easing} from 'react-native';
import * as color from '../utils/colors';
import FlipView from 'react-native-flip-view-next';
import * as helper from '../utils/helpers'


class Quiz extends Component {

    state = {
        currentCard: 0,
        totalCards: 0,
        score: 0,
        isFlipped: false,
        showResult: false
    };

    componentWillMount() {

        const {deck} = this.props.navigation.state.params;
        this.setState({
            totalCards: deck.questions.length,
        });
    }


    flipCard() {
        this.setState({isFlipped: !this.state.isFlipped});

    }

    nextQuestion = (response) => {
        const {currentCard, totalCards} = this.state;
        const newIndex = currentCard + 1;
        if (newIndex < totalCards) {
            this.setState((state) => ({
                currentCard: state.currentCard + 1,
                isFlipped: false,
                score: response ? state.score + 1 : state.score
            }))
        } else {
            this.setState((state) => ({
                score: response ? state.score + 1 : state.score,
                isFlipped: false,
                showResult: true
            }));

            helper.clearLocalNotification().then(helper.setLocalNotification)
        }

    };

    restart = () => {
        this.setState((state) => ({
            currentCard: 0,
            score: 0,
            isFlipped: false,
            showResult: false
        }));
    };

    goBack = () => {
        this.props.navigation.goBack();
    };

    _renderFront() {
        const {deck} = this.props.navigation.state.params;
        const {currentCard} = this.state;

        return (
            <View style={styles.container}>
                <Text style={styles.question}>{deck.questions[currentCard].question}</Text>
                <TouchableOpacity onPress={() => this.flipCard()}>
                    <Text style={styles.answerBtnText}>Answer</Text>
                </TouchableOpacity>
            </View>);
    }

    _renderBack() {
        const {deck} = this.props.navigation.state.params;
        const {currentCard} = this.state;
        return (
            <View style={styles.container}>
                <Text style={styles.question}>{deck.questions[currentCard].answer}</Text>
                <TouchableOpacity onPress={() => this.flipCard()}>
                    <Text style={styles.answerBtnText}>Question</Text>
                </TouchableOpacity>
            </View>);
    }


    render() {

        const {currentCard, totalCards, showResult, score} = this.state;
        const {deck} = this.props.navigation.state.params;

        if (deck.questions.length === 0) {
            return (
                <View style={styles.container}>
                    <Text>This Deck does not have questions yet</Text>
                </View>
            )
        }


        return (

            <View style={{flex: 1}}>
                {!showResult
                    ?
                    <View style={{flex: 1}}>
                        <Text style={styles.counter}>{currentCard + 1}/{totalCards}</Text>
                        <FlipView style={{flex: 1}}
                                  front={this._renderFront()}
                                  back={this._renderBack()}
                                  isFlipped={this.state.isFlipped}
                                  onFlipped={(val) => {
                                      console.log('Flipped: ' + val);
                                  }}
                                  flipAxis="y"
                                  flipEasing={Easing.out(Easing.ease)}
                                  flipDuration={500}
                                  perspective={1000}/>

                        <View style={styles.bottomBtn}>
                            <TouchableOpacity onPress={() => this.nextQuestion(true)}
                                              style={[styles.btn, {backgroundColor: color.green}]}>
                                <Text style={styles.btnText}>Correct</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.nextQuestion(false)}
                                              style={[styles.btn, {backgroundColor: color.red, marginTop: 20}]}>
                                <Text style={styles.btnText}>Incorrect</Text>
                            </TouchableOpacity>
                        </View>
                    </View> :
                    <View style={{flex:1}}>
                        <View style={styles.container}>
                            <Text style={[styles.btnText, {fontSize: 30, color: color.blue}]}>Your
                                score: {((score / totalCards) * 100).toFixed(2)}%</Text>
                        </View>


                        <View style={styles.bottomBtn}>

                            <TouchableOpacity onPress={this.restart}
                                              style={[styles.btn, {backgroundColor: color.green}]}>
                                <Text style={styles.btnText}>Restart</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.goBack}
                                              style={[styles.btn, {backgroundColor: color.red, marginTop: 20}]}>
                                <Text style={styles.btnText}>Finish</Text>
                            </TouchableOpacity>
                        </View>
                    </View>}
            </View>

        );
    }
}


const styles = StyleSheet.create({
    counter: {
        position: "absolute",
        top: 5,
        left: 10,
        fontSize: 15

    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    flipCard: {
        alignItems: 'center',
        justifyContent: 'center',
        backfaceVisibility: 'hidden',
    },
    flipCardBack: {
        position: "absolute",
        top: 0,
    },
    question: {
        textAlign: 'center',
        fontSize: 35,
        marginBottom: 20,
    },
    answerBtnText: {
        textAlign: 'center',
        color: color.red,
        fontSize: 20,
    },
    bottomBtn: {
        bottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        padding: 10,
        width: 200,
        borderRadius: 7,
        height: 45,
        marginLeft: 40,
        marginRight: 40,
    },
    btnText: {
        color: color.white,
        fontSize: 22,
        textAlign: 'center',
    },


});

export default Quiz;
