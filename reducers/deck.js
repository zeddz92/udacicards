import * as actionTypes from '../actions/types';

export function deck(state = {}, action) {

    const payload = action.payload;
    switch (action.type) {

        case actionTypes.RECEIVE_DECKS:
            return payload.decks;

        case actionTypes.ADD_DECK:

            return {
                ...state,
                [payload.title]: {
                    title: payload.title,
                    questions: []
                }
            };

        case actionTypes.ADD_CARD:
            return {
                ...state,
                [payload.title]: {
                    ...state[payload.title],
                    questions: [...state[payload.title].questions, payload.card ]

                }
            };

        default:
            return state;
    }
}

