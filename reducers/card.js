import * as actionTypes from '../actions/types';


export function card(state = {}, action) {

    switch (action.type) {

        case actionTypes.ADD_CARD:
            return {
                ...state,
                [action.title]: {
                    ...state[action.title],
                    questions: [...state[action.title].questions, action.card ]

                }
            };

        default:
            return state;
    }
}

